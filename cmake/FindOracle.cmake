# - Locate Oracle library
# Defines:
#
#  ORACLE_FOUND
#  ORACLE_INCLUDE_DIR
#  ORACLE_INCLUDE_DIRS (not cached)
#  ORACLE_LIBRARY
#  ORACLE_LIBRARIES (not cached)
#  ORACLE_LIBRARY_DIRS (not cached)
#  SQLPLUS_EXECUTABLE
#  ORACLE_BINARY_PATH (not cached)

find_path(ORACLE_INCLUDE_DIR oci.h)
find_library(ORACLE_LIBRARY NAMES clntsh oci)
find_program(SQLPLUS_EXECUTABLE NAMES sqlplus
             HINTS ${ORACLE_INCLUDE_DIR}/../bin)
find_package(Libaio)

# handle the QUIETLY and REQUIRED arguments and set ORACLE_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Oracle DEFAULT_MSG ORACLE_INCLUDE_DIR ORACLE_LIBRARY)

mark_as_advanced(ORACLE_FOUND ORACLE_INCLUDE_DIR ORACLE_LIBRARY SQLPLUS_EXECUTABLE)

set(ORACLE_INCLUDE_DIRS ${ORACLE_INCLUDE_DIR})
get_filename_component(ORACLE_LIBRARY_DIRS ${ORACLE_LIBRARY} PATH)
get_filename_component(ORACLE_BINARY_PATH ${SQLPLUS_EXECUTABLE} PATH)

set(ORACLE_LIBRARIES ${ORACLE_LIBRARY})
