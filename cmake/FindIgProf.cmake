# - Find igprof
# Defines:
#
#  IGPROF_FOUND
#  IGPROF_LIBRARY
#  IGPROF_EXECUTABLE

find_library(IGPROF_LIBRARY NAMES igprof)
find_program(IGPROF_EXECUTABLE NAMES igprof)
get_filename_component(IGPROF_LIBRARY_DIRS ${IGPROF_LIBRARY} PATH)
get_filename_component(IGPROF_BINARY_PATH ${IGPROF_EXECUTABLE} PATH)

find_package(unwind)

# handle the QUIETLY and REQUIRED arguments and set IGPROF_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(IgProf DEFAULT_MSG IGPROF_LIBRARY IGPROF_EXECUTABLE UNWIND_FOUND)

mark_as_advanced(IGPROF_FOUND IGPROF_LIBRARY IGPROF_EXECUTABLE)

set(IGPROF_ENVIRONMENT PACKAGE unwind)

