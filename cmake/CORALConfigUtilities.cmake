# Collection of helpers for CORAL utility configuration

include(CMakeParseArguments)

# coral_add_utility(name [LIBS libs...])
function(coral_add_utility name)
  cmake_parse_arguments(ARG "" "SRCDIR" "LIBS" ${ARGN})
  if(ARG_SRCDIR)
    file(GLOB ${name}_srcs ${CMAKE_CURRENT_SOURCE_DIR}/${ARG_SRCDIR}/*.cpp)
  else()
    file(GLOB ${name}_srcs ${CMAKE_CURRENT_SOURCE_DIR}/utilities/${name}/*.cpp)
  endif()
  if(NOT ${CMAKE_VERSION} VERSION_LESS 3.6.0)
    list(FILTER ${name}_srcs EXCLUDE REGEX "#")
  endif()
  #message(STATUS "Test test_${name} <- ${${name}_srcs} ${${name}_extra_srcs}")
  add_executable(${name} ${${name}_srcs} ${${name}_extra_srcs})
  target_link_libraries(${name} ${ARG_LIBS})
  add_dependencies(${name} PRE_BUILD_BANNER)
  add_dependencies(POST_BUILD_BANNER ${name})
  # Install the test executables
  install(TARGETS ${name} DESTINATION bin)
endfunction()
