#include "MySQL_headers.h"

#include "Cursor.h"
#include "Statement.h"
#include "RelationalAccess/SchemaException.h"

coral::MySQLAccess::Cursor::Cursor( Statement* statement, const coral::AttributeList& rowBuffer ) :
  m_statement( statement ),
  m_rowBuffer( rowBuffer ),
  m_started( false )
{
}


coral::MySQLAccess::Cursor::~Cursor()
{
  this->close();
}


bool
coral::MySQLAccess::Cursor::next()
{
  if ( m_statement == 0 ) return false;
  bool result = m_statement->fetchNext();
  if ( result && !m_started ) m_started=true;  // fix bug #91028
  if ( ! result ) {
    this->close();
  }
  return result;
}


const coral::AttributeList& coral::MySQLAccess::Cursor::currentRow() const
{
  if ( !m_started ) // fix bug #91028
    throw coral::QueryException( "MySQL",
                               "Cursor loop has not started yet",
                               "ICursor::currentRow()" );
  return m_rowBuffer;
}


void
coral::MySQLAccess::Cursor::close()
{
  if ( m_statement != 0 )
  {
    delete m_statement;
    m_statement = 0;
  }
}
