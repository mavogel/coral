#include "CoralBase/Exception.h"
#include "CoralKernel/Property.h"
#include "PropertyManager.h"


coral::PropertyManager::PropertyManager()
  : m_cont()
  , m_mutex()
{
  setDefaultProperties();
}


coral::PropertyManager::~PropertyManager()
{
}


coral::IProperty*
coral::PropertyManager::property(const std::string& key)
{
  {
    // MONITOR_START_CRITICAL
    coral::lock_guard lock( m_mutex );
    std::map<std::string, std::shared_ptr<IProperty> >::iterator loc = m_cont.find(key);
    return loc == m_cont.end() ? NULL : loc->second.get();
    //MONITOR_END_CRITICAL
  }
}


void
coral::PropertyManager::setDefaultProperties()
{
  addProperty("CredentialDatabase", "sqlite_file:coral_credentials.db");
  addProperty("EncryptionKey","");
  addProperty("AuthenticationFile", "authentication.xml");
  addProperty("DBLookupFile", "dblookup.xml");
  addProperty("Server_Hostname", "localhost");
  addProperty("Server_Port", "8080");
}


void
coral::PropertyManager::addProperty( const char* key, const char* value )
{
  std::shared_ptr<IProperty> prop(new Property);
  if ( !prop->set(value) ) // Fix Coverity CHECKED_RETURN
    throw Exception( "Failed to set property", "addProperty", "PropertyManager" );
  m_cont[key] = prop;
}
