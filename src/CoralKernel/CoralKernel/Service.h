#ifndef CORALBASE_SERVICE_H
#define CORALBASE_SERVICE_H 1

// Include files
#include "ILoadableComponent.h"

#include <map>
#include <stdexcept>

namespace coral
{

  class MessageStream;

  class Service : public ILoadableComponent
  {
  protected:

    /// Constructor
    explicit Service( const std::string& name );

    /// Destructor
    virtual ~Service();

    /// Returns the underlying message stream object
    MessageStream& log()
    {
      return *m_log;
    }

    /// Returns the underlying message stream object
    MessageStream& log() const
    {
      return *m_log;
    }

  private:

    /// Copy constructor is private (fix Coverity MISSING_COPY bug #95359)
    Service( const Service& rhs );

    /// Assignment op. is private (fix Coverity MISSING_ASSIGN bug #95359)
    Service& operator=( const Service& rhs );

  private:

    /// The message stream
    mutable MessageStream* m_log;

  };

}
#endif
