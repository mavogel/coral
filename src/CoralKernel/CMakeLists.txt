include(CORALSharedLib)
CORALSharedLib(LIBS lcg_CoralBase ${CMAKE_DL_LIBS}
               TESTS CoralKernelTest
                     ExternalPluginManager
                     PluginList
                     PluginManager
                     PropertyManager)
