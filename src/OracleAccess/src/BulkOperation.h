#ifndef ORACLEACCESS_BULKOPERATION_H
#define ORACLEACCESS_BULKOPERATION_H 1

#include <string>
#include <vector>
#include <memory>
#include "RelationalAccess/IBulkOperation.h"

struct OCIStmt;

namespace coral
{

  class AttributeList;

  namespace OracleAccess
  {

    class SessionProperties;
    class StatementStatistics;
    class PolymorphicVector;

    /**
     * Class BulkOperation
     *
     * Implementation of the IBulkOperation interface
     *///

    class BulkOperation : virtual public coral::IBulkOperation
    {
    public:
      /// Constructor
      BulkOperation( std::shared_ptr<const SessionProperties> properties,
                     const std::string& schemaName,
                     const coral::AttributeList& inputBuffer,
                     int cacheSize,
                     const std::string& statement );

      /// Destructor
      virtual ~BulkOperation();

      /**
       * Processes the next iteration
       *///
      void processNextIteration();

      /**
       * Flushes the data on the client side to the server.
       *///
      void flush();

    private:
      /// Resets the operation and closes the statement handle
      void reset();

      /// Copy constructor is private (fix Coverity MISSING_COPY)
      BulkOperation( const BulkOperation& rhs );

      /// Assignment operator is private (fix Coverity MISSING_ASSIGN)
      BulkOperation& operator=( const BulkOperation& rhs );

    private:

      /// The session properties
      std::shared_ptr<const SessionProperties> m_sessionProperties;

      /// The schema name for this bulk operation
      const std::string m_schemaName;

      /// A reference to the input data buffer
      const coral::AttributeList& m_inputBuffer;

      /// The cache size
      int m_rowsInCache;

      /// The iterations counter
      int m_rowsUsed;

      /// The OCI statement handle
      OCIStmt* m_ociStmtHandle;

      /// The data cache
      std::vector< PolymorphicVector* > m_dataCache;

      /// The statement statistics
      StatementStatistics* m_statementStatistics;
    };

  }

}

#endif
