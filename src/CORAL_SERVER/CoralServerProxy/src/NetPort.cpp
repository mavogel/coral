//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id: NetPort.cpp,v 1.2.2.1 2010-05-26 08:12:41 avalassi Exp $
//
// Description:
//	Class NetPort...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "NetPort.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <exception>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoralServerBase/portmap.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace coral {
namespace CoralServerProxy {

NetPort NetPort::parse(const char* portStr)
{
    std::pair<uint32_t, uint32_t> port = ::coral::parsePort(portStr);
    if (port.first == 0U) {
        if (port.second == 0U) {
            return NetPort();
        } else {
            return NetPort((unsigned short)port.second);
        }
    } else {
        return NetPort(port.first, port.second);
    }
}

void NetPort::print(std::ostream& out) const
{
    if (m_pmapPrognum != 0) {
        out << '[' << m_pmapPrognum << '.' << m_pmapVersion << ']';
    } else {
        out << m_port;
    }
}


} // namespace CoralServerProxy
} // namespace coral
