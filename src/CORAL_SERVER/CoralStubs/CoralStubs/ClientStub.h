#ifndef CORAL_CORALSTUBS_CORALFACADE_H
#define CORAL_CORALSTUBS_CORALFACADE_H 1

#include <string>
#include "CoralServerBase/ICoralFacade.h"
#include "RelationalAccess/AccessMode.h"

namespace coral
{

  // Forward declarations
  class IRequestHandler;

  namespace CoralStubs {

    /** @class ClientStub
     *
     *  Implementation of the ICoralFacade used for the client side.
     *
     *  @author Alexander Kalkhof
     *  @date   2009-01-28
     *///

    class ClientStub : public ICoralFacade
    {
    public:

      // Destructor.
      virtual ~ClientStub();

      // Constructor from an IRequestHandler.
      ClientStub( IRequestHandler& requestHandler );

      void setCertificateData( const coral::ICertificateData* cert ) override;

      //connect to a database backend
      Token connect( const std::string& dbUrl,
              const coral::AccessMode mode,
              bool& fromProxy ) const override;

      //release the session and release the database connection
      void releaseSession( coral::Token sessionID ) const override;

      const std::vector<std::string> fetchSessionProperties( Token sessionID ) const override;

      void startTransaction( coral::Token sessionID,
              bool readOnly = false ) const override;

      void commitTransaction( coral::Token sessionID ) const override;

      void rollbackTransaction( coral::Token sessionID ) const override;

      const std::set<std::string> listTables( Token sessionID,
              const std::string& schemaName ) const override;

      bool existsTable( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName ) const override;

      const TableDescription fetchTableDescription( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName ) const override;

      const std::set<std::string> listViews( Token sessionID,
              const std::string& schemaName ) const override;

      bool existsView( Token sessionID, const std::string& schemaName,
              const std::string& viewName ) const override;

      const std::pair<TableDescription,std::string>
      fetchViewDescription( Token sessionID,
              const std::string& schemaName,
              const std::string& viewName ) const override;

      IRowIteratorPtr fetchRows( Token sessionID,
              const QueryDefinition& qd,
              AttributeList* pRowBuffer,
              unsigned int rowCacheSize,
              unsigned int memoryCacheSize ) const override;

      IRowIteratorPtr fetchRows( Token sessionID,
              const QueryDefinition& qd,
              const std::map< std::string, std::string > outputTypes,
              unsigned int rowCacheSize,
              unsigned int memoryCacheSize ) const override;

      IRowIteratorPtr fetchAllRows( Token sessionID,
              const QueryDefinition& qd,
              AttributeList* rowBuffer ) const override;

      IRowIteratorPtr fetchAllRows( Token sessionID,
              const QueryDefinition& qd,
              const std::map< std::string,
              std::string > outputTypes ) const override;

      void callProcedure( Token sessionID,
              const std::string& schemaName,
              const std::string& procedureName,
              const coral::AttributeList& inputArguments ) const override;

      /*
      int deleteTableRows( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName,
              const std::string& whereClause,
              const std::string& whereData ) const override;

      const std::string formatRowBufferAsString( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName ) const override;

      void insertRowAsString( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName,
              const std::string& rowBufferAS ) const override;

      Token bulkInsertAsString( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName,
              const std::string& rowBufferAS,
              int rowCacheSizeDb ) const override;

      void releaseBulkOp( Token bulkOpID ) const override;

      void processRows( Token bulkOpID,
              const std::vector<coral::AttributeList>& rowsAS ) const override;

      void flush( Token bulkOpID ) const override;
      */

    private:

      // Reference to an IRequestHandler instance
      IRequestHandler& m_requestHandler;

    };

  }

}

#endif
