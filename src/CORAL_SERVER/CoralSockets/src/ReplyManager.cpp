// Include files
#include "CoralBase/../src/coral_mutex_headers.h"
#include "CoralSockets/GenericSocketException.h"

// Local include files
#include "ReplyManager.h"

// Logger
#define LOGGER_NAME "CoralSockets::ReplyManager"
#include "logger.h"

// Namespace
using namespace coral;
using namespace coral::CoralSockets;

// Assign value of static variable
unsigned int ReplySlot::timeout=30*60; // half an hour

//-----------------------------------------------------------------------------

void ReplySlot::appendReply( ByteBufferPtr reply,
                             int segmentNr,
                             bool lastReply )
{
  coral::lock_guard lock( m_mutex);
  if ( !m_used )
    throw GenericSocketException("Slot is not used!",
                                 "ReplySlot::appendReply");
  if ( m_status==ALL_REPLIES )
    throw GenericSocketException("Slot has already got all replies!",
                                 "ReplySlot::appendReply");
  if ( m_segmentNr != segmentNr )
    throw SegmentationErrorException("multi segment reply, but segment no is"
                                     " not continous!",
                                     "ReplySlot::appendReply");
  if ( m_status==ITERATOR_CLOSED )
  {
    // if the iterator is already closed we drop incoming replies after the
    // checks
    if ( lastReply )
      _freeSlot();
    return;
  }
  m_replies.push( std::move(reply) );
  m_segmentNr++;
  if ( lastReply ) m_status = ALL_REPLIES;
  m_cond.notify_all();
}

//-----------------------------------------------------------------------------

ByteBufferPtr ReplySlot::nextReply( bool& lastReply )
{
  coral::unique_lock lock( m_mutex );
  if ( m_status == ITERATOR_CLOSED )
    throw GenericSocketException("Panic! nextReply called after slot has"
                                 " been released!", "ReplySlot::nextReply");
  // return 0 if the last reply has already been returned
  if ( m_status == ALL_REPLIES && m_replies.empty() )
    return ByteBufferPtr();

  using clock = std::chrono::steady_clock;
  auto now = clock::now();
  auto wait_until = now + std::chrono::seconds(timeout);
  while ( m_replies.empty() && m_status == OK )
  {
    // it looks like on Mac OS signals can get lost? so better use a time out..
    // todo: handle errors (connection lost etc...)
    // wait until timeout in intervals of 20 seconds or less
    auto next_wait_until = std::min(now + std::chrono::seconds(20), wait_until);
    if ( m_cond.wait_until(lock, next_wait_until) == std::cv_status::timeout )
    {
      DEBUG( "wait for reply timed out..."<< m_requestID << std::endl);
      if (!m_replies.empty() )
        ERROR("(non fatal) wait for reply timed out but reply is there.."
              " Signal lost? " << m_requestID);
    }
    now = clock::now();
    if ( now > wait_until )
      setTimedOut();
  }
  if ( m_status == TIME_OUT )
    throw GenericSocketException("reply timed out","ReplySlot::nextReply");
  if ( m_status == CLOSED )
    throw GenericSocketException("connection closed", "ReplySlot::nextReply");
  if ( m_replies.empty() )
    throw GenericSocketException("Panic! replies empty before pop!",
                                 "ReplySlot::nextReply");
  ByteBufferPtr buffer( std::move(m_replies.front()) );
  m_replies.pop();
  lastReply = m_replies.empty() && (m_status==ALL_REPLIES);
  return buffer;
}

//-----------------------------------------------------------------------------

void ReplySlot::_freeSlot()
{
  DEBUG("freeSlot("<<m_requestID<<") called" << std::endl);
  while ( !m_replies.empty() )
    m_replies.pop();
  m_used=false;
  m_status=OK;
}

//-----------------------------------------------------------------------------

void ReplySlot::freeSlot()
{
  coral::lock_guard lock( m_mutex );
  _freeSlot();
}

//-----------------------------------------------------------------------------

void ReplySlot::releaseSlot()
{
  coral::lock_guard lock( m_mutex );
  DEBUG("releaseSlot("<<m_requestID<<") called" << std::endl);
  // the iterator has been closed, so we don't need the replies
  // any more...
  while ( !m_replies.empty() )
    m_replies.pop();
  if (m_status==ALL_REPLIES)
  {
    // we have already got all replies, so we can just free the slot
    _freeSlot();
    return;
  }
  // wait until all replies have arrived and then free the slot
  m_status=ITERATOR_CLOSED;
}

//-----------------------------------------------------------------------------

bool ReplySlot::takeSlot( int requestID )
{
  coral::lock_guard lock( m_mutex );
  DEBUG("takeSlot( " << requestID << " ) called" << std::endl );
  if ( m_used ) return false;
  m_requestID=requestID;
  m_used=true;
  m_status=OK;
  m_segmentNr=0;
  return true;
}

//-----------------------------------------------------------------------------

void ReplySlot::setTimedOut()
{
  // private method, expects m_mutex to be locked!
  if (m_status != OK)
    throw GenericSocketException("Panic! m_status!=OK",
                                 "ReplySlot::setTimedOut");
  m_status=TIME_OUT;
}

//-----------------------------------------------------------------------------

void ReplySlot::setConnectionClosed()
{
  coral::lock_guard lock( m_mutex );
  m_status=CLOSED;
  m_cond.notify_all();
}

//-----------------------------------------------------------------------------

SocketReplyIterator::SocketReplyIterator( std::shared_ptr<ReplySlot> const& slot )
  : IByteBufferIterator()
  , m_slot(slot)
  , m_slotReleased(false)
  , m_currentBuf((ByteBuffer*) 0)
  , m_lastReply( false )
{
}

//-----------------------------------------------------------------------------

SocketReplyIterator::~SocketReplyIterator()
{
  if ( !m_slotReleased ) {
    auto slot = m_slot.lock();
    if (slot != nullptr) {
      slot->releaseSlot();
    }
  }
}

//-----------------------------------------------------------------------------

bool SocketReplyIterator::nextBuffer()
{
  auto slot = m_slot.lock();
  if (m_slotReleased || slot == nullptr)
  {
    // slot got already released, we are at the last buffer (or even beyond)
    // release previous buffer
    m_currentBuf = std::shared_ptr<ByteBuffer>( (ByteBuffer*)0 );
    return false;
  }
  DEBUG( "SocketReplyIterator::nextBuffer() request #"<<slot->requestID() );
  m_currentBuf = slot->nextReply( m_lastReply );
  if (m_lastReply)
  {
    slot->releaseSlot();
    m_slotReleased=true;
  }
  return m_currentBuf.get() != 0;
}

//-----------------------------------------------------------------------------

bool SocketReplyIterator::isLastBuffer() const
{
  if ( m_currentBuf.get() == 0)
    throw GenericSocketException("no current Buffer. NextBuffer has not"
                                 " been called?", "SocketReplyIterator::isLastBuffer()");
  return m_lastReply;
}

//-----------------------------------------------------------------------------

const ByteBuffer& SocketReplyIterator::currentBuffer() const
{
  if ( m_currentBuf.get() == 0)
    throw GenericSocketException("called currentReply() before the first "
                                 "nextBuffer() or after the last reply", "SocketReplyIterator::currentReply");
  return *m_currentBuf;
}

//-----------------------------------------------------------------------------

ReplyManager::ReplyManager(int sleepersSize) :
  m_requestOffset(0),
  m_answerOffset(0),
  m_sleepersSize(sleepersSize)
{
    // allocate array, make sure we correctly delete it
    m_sleepers.reset(new ReplySlot[m_sleepersSize], std::default_delete<ReplySlot[]>());
}

//-----------------------------------------------------------------------------

ReplyManager::~ReplyManager()
{
}

//-----------------------------------------------------------------------------

std::shared_ptr<ReplySlot> ReplyManager::requestSlot( int requestID )
{
  //SCOPED_TIMER( "CoralSockets::ReplyManager::requestSlot" );
  int slot = (requestID + m_requestOffset) % m_sleepersSize;
  int tries=0;
  int maxTries=m_sleepersSize;
  auto sleepers = m_sleepers.get();
  while ( sleepers[slot].used() != 0 && tries< maxTries )
  {
    tries++;
    DEBUG( "skipping slot " << slot << " for id " << requestID);
    nextSlot( slot );
  }
  if ( tries>=maxTries )
    throw AllSlotsTakenException( "requestSlot" );
  if ( !sleepers[slot].takeSlot( requestID ) )
    throw GenericSocketException("PANIC! internal error could not take slot!",
                                 "ReplyManager::requestSlot");
  m_requestOffset=(m_requestOffset+tries)%m_sleepersSize;
  return std::shared_ptr<ReplySlot>(m_sleepers, sleepers+slot);
}

//-----------------------------------------------------------------------------

std::shared_ptr<ReplySlot> ReplyManager::findSlot( int requestID )
{
  //SCOPED_TIMER( "CoralSockets::ReplyManager::findSlot" );
  int slot = (requestID + m_answerOffset ) % m_sleepersSize;
  int tries=0;
  int maxTries=m_sleepersSize;
  auto sleepers = m_sleepers.get();
  if ( sleepers[slot].requestID() == requestID )
    return std::shared_ptr<ReplySlot>(m_sleepers, sleepers+slot);
  if ( sleepers[slot].requestID() > requestID )
  {
    while ( sleepers[slot].requestID() != requestID && tries< maxTries )
    {
      tries++;
      DEBUG( "prev answer skipping slot " << slot << " id "
             << sleepers[slot].requestID() << " searching " << requestID );
      prevSlot(slot);
    }
  }
  else
  {
    while ( sleepers[slot].requestID() != requestID && tries< maxTries )
    {
      tries++;
      DEBUG( "answer skipping slot " << slot <<" id "
             << sleepers[slot].requestID() << " searching " << requestID );
      nextSlot(slot);
    }
    m_answerOffset=(m_answerOffset+tries)%m_sleepersSize;
  }
  if ( sleepers[slot].requestID() != requestID )
    throw GenericSocketException( " didn't find correct slot!",
                                  "ReplyManager::findSlot()");
  if ( tries>=maxTries )
    throw GenericSocketException( "replySlot max tries" );
  return std::shared_ptr<ReplySlot>(m_sleepers, sleepers+slot);
}

//-----------------------------------------------------------------------------

void ReplyManager::close()
{
  for ( int i=0; i < m_sleepersSize; i++)
    m_sleepers.get()[i].setConnectionClosed();
}

//-----------------------------------------------------------------------------
