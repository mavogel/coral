#ifndef CORALSERVERBASE_PORTMAP_H
#define CORALSERVERBASE_PORTMAP_H 1

#include <string>
#include <stdint.h>

namespace coral {

/*
 * Parse port specification.
 *
 * Port can be specified either as integer port number or RPC program number
 * and optional version number. If RPC program number is specified then
 * server will be listening on arbitrary port but it will register that
 * port in portmapper.
 *
 * Format for RPC syntax:
 *  - opening square bracket
 *  - program number, standard C integer syntax (0x prefix for hex)
 *  - optional dot and version (default is 1)
 *    - version can be a number (again standard C syntax)
 *    - or a string starting with '#' followed by any characters,
 *      string will be hashed and its hash value will be used for version
 *  - closing square bracket
 *
 * Examples: 1234 [1000] [1000.23] [0xbadc0de.0x100] [123.#SomeString]
 *
 * Returns pair of 32-bit unsigned integers, if port is specified as a number
 * then first integer will be 0, and second integer is a port number. If RPC
 * format is used then first number is a program number and second number is
 * program version. In case of errors both returned integers will be 0.
 */
std::pair<uint32_t, uint32_t> parsePort(const std::string& portSpec);

/*
 * Register port in a portmapper.
 *
 * If `lockDir` is non-empty it tries to create a lock file in the `lockDir`
 * directory whose name encodes program and version number and obtains an
 * exclusive lock on that file.  If this fails and `override` is false then
 * exception is thrown, otherwise it writes PID to a file and registers
 * port in portmapper overriding any existing mapping.
 *
 * If `lockDir` is empty then lock file is not created and lock is not
 * obtained. If `override` is true then any existing registration of the
 * same program/version number is removed. New registration is attempted
 * and if it fails an exception is thrown.
 *
 * Returns file descriptor of a lock file if `lockDir` is given, or -1
 * otherwise.
 *
 * A subclass of CoralServerBaseException is thrown on errors.
 */
int pmapRegister(unsigned short port, unsigned long rpc_prognum,
                 unsigned long rpc_version, bool override,
                 const std::string& lockDir);

/*
 * Unregister port mapping.
 *
 * If `port` is given and it is non-zero then unregister only if
 * currently registered port has the same number.
 *
 * if `lockfd` is given and is non-negative then the file descriptor
 * is closed (removing the lock obtained by `pmapRegister()`)
 *
 * Returns true on success, false if there was no prior registration
 * or when ports do not match.
 *
 * Throws exception if RPC call fails.
 */
bool pmapUnregister(unsigned long rpc_prognum, unsigned long rpc_version,
                    unsigned short port=0, int lockfd=-1);

/*
 * Get associated port number.
 *
 * Returns 0 on failure.
 */
unsigned short pmapGetPort(uint32_t host_ip, unsigned long rpc_prognum, unsigned long rpc_version);

/*
 * Get associated port number, overload of the above method for host name.
 *
 * Throws exception if host name cannot be resolved.
 *
 * Returns 0 on portmapper resolution failure.
 */
unsigned short pmapGetPort(const std::string& host, unsigned long rpc_prognum, unsigned long rpc_version);

}

#endif // CORALSERVERBASE_PORTMAP_H
