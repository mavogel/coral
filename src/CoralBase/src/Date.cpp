// Include files
#include "CoralBase/Date.h"
#include "CoralBase/TimeStamp.h"

namespace coral
{

  Date::Date()
  {
    coral::TimeStamp now = coral::TimeStamp::now(); // UTC
    m_year = now.year();
    m_mon = now.month();
    m_day = now.day();
  }

  Date::Date( const Date& rhs )
    : m_year( rhs.m_year ), m_mon( rhs.m_mon ), m_day( rhs.m_day )
  {
  }

  Date::Date( int year, int month, int day )
    : m_year( year ), m_mon ( month ), m_day ( day )
  {
  }

  Date::~Date()
  {
  }

  Date& Date::operator= ( const Date& rhs )
  {
    m_year = rhs.m_year;
    m_mon = rhs.m_mon;
    m_day = rhs.m_day;
    return *this;
  }

  Date::Date( const std::chrono::system_clock::time_point& ptime )
  {
    coral::TimeStamp ts( ptime );
    m_year = ts.year();
    m_mon = ts.month();
    m_day = ts.day();
  }

  std::chrono::system_clock::time_point Date::time() const
  {
    return coral::TimeStamp( m_year, m_mon, m_day, 0, 0, 0, 0 ).time();
  }

}
