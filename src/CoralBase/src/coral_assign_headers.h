#ifndef CORALBASE_CORALASSIGNHEADERS_H
#define CORALBASE_CORALASSIGNHEADERS_H 1

// NB: coral_assign_headers should be _completely_ removed in all branches!

// FIXME! Should use c++11 instead of boost in the internal implementation!
// NB: boost_assign_headers should be _completely_ removed in CORAL3 branch!
#include "CoralBase/../src/boost_assign_headers.h" // SHOULD BE REMOVED!

#endif // CORALBASE_CORALASSIGNHEADERS_H
