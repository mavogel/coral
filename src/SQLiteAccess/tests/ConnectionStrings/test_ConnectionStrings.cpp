#include <iostream>
#include <memory>
#include "CoralBase/../tests/Common/CoralCppUnitTest.h"
#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/ConnectionServiceException.h"
#include "CoralCommon/ISession.h"
#include <boost/filesystem.hpp>
namespace coral
{
  class ConnectionStrings;
}

class coral::ConnectionStrings : public coral::CoralCppUnitTest
{

  CPPUNIT_TEST_SUITE(ConnectionStrings);
  CPPUNIT_TEST(test_All);
  CPPUNIT_TEST_SUITE_END();

private:
  // The connection string for CORAL tests
  std::string m_coralConnStringAdmin;

public:
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void
  setUp()
  {
  }

  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void
  test_All()
  {
    std::cout << "Starting test ConnectionStrings " << std::endl;
    char *user = ::getenv("USER");
    char name[] = "SQLITE_FILE_PATH";
    char goodsqlpath[] = "SQLITE_FILE_PATH=/tmp/test_ConnectionStrings/SQLITE_FILE_PATH";
    char wrongsqlpath[] = "SQLITE_FILE_PATH=/tmp/test_ConnectionStrings/SQLITE_FILE_PATH/wrong";
    ::unsetenv(name);
    boost::filesystem::create_directories("/tmp/test_ConnectionStrings");
    boost::filesystem::create_directories("/tmp/test_ConnectionStrings/absolute_path");
    boost::filesystem::create_directories("/tmp/test_ConnectionStrings/SQLITE_FILE_PATH");


    //check with full path, without existing SQLITE_FILE_PATH
    std::string basename="sqlite_file:/tmp/test_ConnectionStrings/absolute_path" + (user ? std::string(user) : std::string("USER")) + "_" + BuildUniqueTableName("");
    m_coralConnStringAdmin = basename + ".db";
    connect_to_db();
    std::cout << "Connection to a sqlite database with a full path and no SQLITE_FILE_PATH worked as expected" << std::endl;

    //check with full path, with existing SQLITE_FILE_PATH
    ::putenv(goodsqlpath);
    m_coralConnStringAdmin = basename + "2.db";
    connect_to_db();
    std::cout << "Connection to a sqlite database with a full path and a valid SQLITE_FILE_PATH worked as expected" << std::endl;

    // with wrong SQLITE_FILE_PATH
    ::putenv(wrongsqlpath);
    m_coralConnStringAdmin = basename + "3.db";
    connect_to_db();
    std::cout << "Connection to a sqlite database with a full path and an invalid SQLITE_FILE_PATH worked as expected" << std::endl;

    //check with relative path if SQLITE_FILE_PATH is set
    basename="sqlite_file:" + (user ? std::string(user) : std::string("USER")) + "_" + BuildUniqueTableName("");
    m_coralConnStringAdmin = basename + "4.db";
    std::cout << "m_coralConnStringAdmin " << m_coralConnStringAdmin << std::endl;
    ::unsetenv(name);
    connect_to_db();
    std::cout << "Connection to a sqlite database with a relative path and no SQLITE_FILE_PATH worked as expected" << std::endl;

    //check with relative path without SQLITE_FILE_PATH
    ::putenv(goodsqlpath);
    m_coralConnStringAdmin = basename + "5.db";
    connect_to_db();
    std::cout << "Connection to a sqlite database with a relative path and a valid SQLITE_FILE_PATH worked as expected" << std::endl;

    //check with relative path with wrong SQLITE_FILE_PATH
    ::putenv(wrongsqlpath);
    m_coralConnStringAdmin = basename + "6.db";
    connect_to_db(true);
    std::cout << "Connection to a sqlite database with a relative path and an invalid SQLITE_FILE_PATH worked as expected" << std::endl;
  }
  void connect_to_db(bool should_fail = false )
  {

    try
    {
      coral::ConnectionService connSvc;
      char *sqlpath = ::getenv("SQLITE_FILE_PATH");
      std::cout << "connecting to " <<m_coralConnStringAdmin<< " with SQLITE_FILE_PATH="<< ( sqlpath ? std::string( sqlpath ) : std::string( "unset" ) ) << std::endl;
      std::auto_ptr<coral::ISessionProxy> session(connSvc.connect(m_coralConnStringAdmin));
      // if we get there it means we managed to create a sqlite file
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "expected to fail but passed", should_fail, false );
    }
    catch (const coral::ConnectionServiceException &exc)
    {
      std::cout << "sqlpath Exception caught, cannot connect to m_coralConnStringAdmin:" << m_coralConnStringAdmin << " Exception is:" << exc.what() << std::endl;
      // if we get there it means we couldn't create a sqlite file
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "expected to succeed but failed", should_fail, true );
    }
  }
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void
  tearDown()
  {
    char *user = ::getenv("USER");
    boost::filesystem::remove((user ? std::string(user) : std::string("USER")) + "_" + BuildUniqueTableName("")+ "4.db");
    boost::filesystem::remove_all("/tmp/test_ConnectionStrings");
  }
};

CPPUNIT_TEST_SUITE_REGISTRATION(coral::ConnectionStrings);

CORALCPPUNITTEST_MAIN(ConnectionStrings)
