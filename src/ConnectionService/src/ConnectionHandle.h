#ifndef CONNECTIONSERVICE_CONNECTIONHANDLE_H
#define CONNECTIONSERVICE_CONNECTIONHANDLE_H 1

#include <memory>
#include "CoralBase/TimeStamp.h"
#include "CoralKernel/RefCounted.h"
#include "RelationalAccess/IMonitoringService.h"
#include "ServiceSpecificConfiguration.h"

namespace coral
{

  class IConnection;
  class ISession;
  class ITypeConverter;

  namespace ConnectionService
  {

    /// ConnectionService guid definition
    typedef std::string Guid;

    /// convenience object to store the info shared on all the handles
    class ConnectionSharedInfo
    {
    public:
      /// constructor
      ConnectionSharedInfo(const std::string& connectionServiceName) :
        m_connectionId(""),
        m_serviceName(""),
        m_open(false),
        m_startIdle(coral::TimeStamp::now()),
        m_idle(true),
        m_technologyName(""),
        m_configuration(),
        m_connectionServiceName(connectionServiceName){}
      /// constructor
      ConnectionSharedInfo(const ConnectionParams& connectionParams,
                           const ConnectionServiceConfiguration& configuration) :
        m_connectionId(""),
        m_serviceName(connectionParams.serviceName()),
        m_open(false),
        m_startIdle(coral::TimeStamp::now()),
        m_idle(true),
        m_technologyName(connectionParams.technologyName()),
        m_connString( connectionParams.connectionString() ),
        m_configuration(connectionParams,configuration ),
        m_connectionServiceName(configuration.serviceName()){}
      /// destructor
      virtual ~ConnectionSharedInfo(){}
      /// the unique identifier of the connection
      Guid m_connectionId;
      /// the service name
      std::string m_serviceName;
      /// the connection open flag
      bool m_open;
      /// the start time of idle status
      coral::TimeStamp m_startIdle;
      /// the idle flag;
      bool m_idle;
      /// the db technology of the underlying connection
      std::string m_technologyName;
      /// connection string
      std::string m_connString;
      /// the service configuration
      ServiceSpecificConfiguration m_configuration;
      /// the connection service name
      std::string m_connectionServiceName;
    };

    /// handle of the ISession object used internally in the proxy.
    class ConnectionHandle
    {

    public:

      /// constructor
      ConnectionHandle( const std::string& connectionServiceName );

      /// constructor
      ConnectionHandle( coral::IConnection* connection,
                        const ConnectionParams& connectionParams,
                        const ConnectionServiceConfiguration& configuration );

      /// destructor
      virtual ~ConnectionHandle();

      /// copy constructor
      ConnectionHandle( const ConnectionHandle& rhs );

      /// assignment operator
      ConnectionHandle& operator=( const ConnectionHandle& rhs );

      /// returns
      operator bool() const;

      // initialize the connection
      bool open();

      /*
      /// THIS IS NEVER CALLED! (AV 22.07.2010)
      /// finalize (and invalidate) the connection.
      bool close();
      *///

      /// returns true if the connection is in open state
      bool isOpen() const;

      /// returns true if the connection is still valid (probed)
      bool isValid() const;

      /// returns a new session
      coral::ISession* newSession(const std::string& schemaName,
                                  const std::string& userName,
                                  const std::string& password,
                                  coral::AccessMode accessMode,
                                  const std::string& sessionId );

      /// returns the connection id
      Guid connectionId() const;

      /// returns the actual connection string
      std::string connectionString() const;

      /// returns the service name
      const std::string& serviceName() const;

      /// returns the technology name
      const std::string& technologyName() const;

      /// returns the number of session opened
      size_t numberOfSessions() const;

      /// returns the server version
      coral::ITypeConverter& typeConverter() const;

      /// returns the server version
      std::string serverVersion() const;

      /// sets the start of idle time
      void setIdle();

      /// retrieves the idle flag
      bool isIdle() const;

      /// returns true if timeout is reached
      bool isExpired() const;

      /// returns the specific time-out of the connection
      int specificTimeOut();

      /// is monitoring enabled for this connection?
      bool isMonitoringEnabled(); // CORALCOOL-2941
      
    private:

      /// default constructor
      ConnectionHandle();

      /// Shared pointer to connection
      std::shared_ptr<IConnection> m_connection;

      /// Shared pointer to connection info
      std::shared_ptr<ConnectionSharedInfo> m_info;

      /// Handle (shared pointer) to monitoring service
      /// The monitoring service must survive the connection (CORALCOOL-2944)
      coral::IHandle<coral::monitor::IMonitoringService> m_monitoringService;
      
    };

  }

}

#endif
