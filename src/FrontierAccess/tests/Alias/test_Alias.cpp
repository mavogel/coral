#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/MessageStream.h"
#include "CoralBase/../tests/Common/CoralCppUnitDBTest.h"

#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ITable.h"
#include "RelationalAccess/ITableDataEditor.h"
#include "RelationalAccess/ITablePrivilegeManager.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/TableDescription.h"
#include "RelationalAccess/IWebCacheControl.h"

#include <iostream>
#include <sstream>

// The connection strings for CORAL tests
const std::string UrlRW = coral::CoralCppUnitDBTest::BuildUrl( "Oracle", false );
const std::string UrlRO = coral::CoralCppUnitDBTest::BuildUrl( "Frontier", true );

// The table names for CORAL tests
const std::string TABLE1 = coral::CoralCppUnitTest::BuildUniqueTableName( "T1" );
const std::string TABLE2 = coral::CoralCppUnitTest::BuildUniqueTableName( "T2" );

void readWithAlias( std::auto_ptr<coral::ISessionProxy>& sessionR, const std::string& alias )
{
  std::cout << "__TEST Read with table alias '" << alias << "'" << std::endl;
  try {
    std::auto_ptr<coral::IQuery> query( sessionR->nominalSchema().newQuery() );

    if( alias.empty() )
    {
      query->addToTableList( TABLE1 );
      query->addToOutputList( "VALUE" );
    }
    else
    {
      query->addToTableList( TABLE1, alias );
      query->addToOutputList( alias + ".VALUE" );
    }
    std::string whereClause = "ID='1'";
    if ( alias != "" ) whereClause = alias + "." + whereClause;
    coral::AttributeList whereData;
    query->setCondition( whereClause, whereData );
    coral::MsgLevel lvl = coral::MessageStream::msgVerbosity();
    coral::MessageStream::setMsgVerbosity( coral::Debug );
    try
    {
      coral::ICursor& cursor = query->execute();
      int nRows = 0;
      while ( cursor.next() ) {
        nRows++;
        std::stringstream out;
        cursor.currentRow().toOutputStream( out );
        std::cout << "Row #" << nRows << " found: " << out.str() << std::endl;
      }
      std::cout << "Retrieved " << nRows << " rows" << std::endl;
    }
    catch(...)
    {
      coral::MessageStream::setMsgVerbosity( lvl );
      throw;
    }
    coral::MessageStream::setMsgVerbosity( lvl );
  }
  catch ( std::exception& e ) {
    std::cerr << "Standard C++ exception caught: " << e.what() << std::endl;
    throw; // The test must fail if we get an exception!
  }
}

void readCmsAlias( std::auto_ptr<coral::ISessionProxy>& sessionR, const std::string& alias )
{
  std::cout << "__TEST Read with table alias '" << alias << "'" << std::endl;
  try
  {
    std::string treetablename = alias;
    std::auto_ptr<coral::IQuery> query( sessionR->nominalSchema().newQuery() );
    query->addToTableList( treetablename, "p1" );
    query->addToTableList( treetablename, "p2" );
    query->addToOutputList( "p1.tagid" );
    query->setRowCacheSize( 100 );
    coral::AttributeList bindData;
    bindData.extend( "nodelabel",typeid(std::string) );
    bindData["nodelabel"].data<std::string>()= "node 2";
    bindData.extend( "tagid",typeid(unsigned int) );
    bindData["tagid"].data<unsigned int>()=0;
    query->setCondition( "p1.lft BETWEEN p2.lft AND p2.rgt AND p2.nodelabel = :nodelabel AND p1.tagid <> :tagid", bindData );
    coral::AttributeList qresult;
    qresult.extend("tagid", typeid(unsigned int));
    query->defineOutput(qresult);
    std::vector<unsigned int> leaftagids;
    leaftagids.reserve(100);
    coral::MsgLevel lvl = coral::MessageStream::msgVerbosity();
    coral::MessageStream::setMsgVerbosity( coral::Debug );
    try
    {
      coral::ICursor& cursor = query->execute();
      while( cursor.next() )
      {
        const coral::AttributeList& row = cursor.currentRow();
        leaftagids.push_back(row["tagid"].data<unsigned int>());
      }
      cursor.close();
      std::cout << "Retrieved " << leaftagids.size() << " leaftagids" << std::endl;
    }
    catch(...)
    {
      coral::MessageStream::setMsgVerbosity( lvl );
      throw;
    }
    coral::MessageStream::setMsgVerbosity( lvl );
  }
  catch ( std::exception& e )
  {
    std::cerr << "Standard C++ exception caught: " << e.what() << std::endl;
    throw; // The test must fail if we get an exception!
  }
}

int main( int, char** )
{
  try
  {
    coral::ConnectionService connSvc;

    // 1. Write sample data
    // COOL bug report due to an table alias error
    std::string connectW = UrlRW;
    std::cout << "_TEST Write sample data" << std::endl;
    coral::AccessMode accessModeW = coral::Update;
    std::auto_ptr<coral::ISessionProxy>
      sessionW( connSvc.connect( connectW, accessModeW ) );
    sessionW->transaction().start( false );
    coral::ISchema& schema = sessionW->nominalSchema();
    schema.dropIfExistsTable( TABLE1 );
    {
      coral::TableDescription description;
      description.setName( TABLE1 );
      description.insertColumn
        ( "ID", coral::AttributeSpecification::typeNameForId
          ( typeid(std::string) ) );
      description.insertColumn
        ( "VALUE", coral::AttributeSpecification::typeNameForId
          ( typeid(std::string) ) );
      coral::ITable& table = schema.createTable( description );
      coral::AttributeList rowBuffer;
      table.dataEditor().rowBuffer( rowBuffer );
      rowBuffer["ID"].data<std::string>() = "1";
      rowBuffer["VALUE"].data<std::string>() = "Value for 1";
      table.dataEditor().insertRow( rowBuffer );
      rowBuffer["ID"].data<std::string>() = "2";
      rowBuffer["VALUE"].data<std::string>() = "Value for 2";
      table.dataEditor().insertRow( rowBuffer );
      table.privilegeManager().grantToUser( "PUBLIC", coral::ITablePrivilegeManager::Select );
      // CMS table self-alias bug report, see: https://savannah.cern.ch/bugs/index.php?34576 aka CORALCOOL-521
      schema.dropIfExistsTable( TABLE2 ); // "TAGTREE_TABLE"
      {
        coral::TableDescription ttd;
        ttd.setName( TABLE2 );
        ttd.insertColumn(         "nodeid",      coral::AttributeSpecification::typeNameForId( typeid(unsigned) ) );
        ttd.setPrimaryKey(        "nodeid" );
        ttd.insertColumn(         "nodelabel",   coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
        ttd.setNotNullConstraint( "nodelabel",   true );
        ttd.setUniqueConstraint(  "nodelabel" );
        ttd.insertColumn(         "lft",         coral::AttributeSpecification::typeNameForId( typeid(unsigned) ) );
        ttd.setNotNullConstraint( "lft",         true );
        ttd.insertColumn(         "rgt",         coral::AttributeSpecification::typeNameForId( typeid(unsigned) ) );
        ttd.setNotNullConstraint( "rgt",         true );
        ttd.insertColumn(         "parentid",    coral::AttributeSpecification::typeNameForId( typeid(unsigned) ) );
        ttd.setNotNullConstraint( "parentid",    true );
        ttd.insertColumn(         "tagid",       coral::AttributeSpecification::typeNameForId( typeid(unsigned) ) );
        ttd.insertColumn(         "globalsince", coral::AttributeSpecification::typeNameForId( typeid(unsigned long long ) ) );
        ttd.insertColumn(         "globaltill",  coral::AttributeSpecification::typeNameForId( typeid(unsigned long long ) ) );
        coral::ITable& tabttd = schema.createTable( ttd );
        coral::AttributeList rowBuffer;
        tabttd.dataEditor().rowBuffer( rowBuffer );
        rowBuffer["nodeid"   ].data<int>() = 1;
        rowBuffer["nodelabel"].data<std::string>() = "root";
        rowBuffer["lft"      ].data<int>() = 2;
        rowBuffer["rgt"      ].data<int>() = 3;
        rowBuffer["parentid" ].data<int>() = 0;
        rowBuffer["tagid"    ].data<int>() = 1;
        tabttd.dataEditor().insertRow( rowBuffer );
        rowBuffer["nodeid"   ].data<int>() = 2;
        rowBuffer["nodelabel"].data<std::string>() = "node 2";
        rowBuffer["lft"      ].data<int>() = 0;
        rowBuffer["rgt"      ].data<int>() = 0;
        rowBuffer["parentid" ].data<int>() = 1;
        rowBuffer["tagid"    ].data<int>() = 1;
        tabttd.dataEditor().insertRow( rowBuffer );
        rowBuffer["nodeid"   ].data<int>() = 3;
        rowBuffer["nodelabel"].data<std::string>() = "node 3";
        rowBuffer["lft"      ].data<int>() = 0;
        rowBuffer["rgt"      ].data<int>() = 0;
        rowBuffer["parentid" ].data<int>() = 1;
        rowBuffer["tagid"    ].data<int>() = 1;
        tabttd.dataEditor().insertRow( rowBuffer );
        tabttd.privilegeManager().grantToUser( "PUBLIC", coral::ITablePrivilegeManager::Select );
      }
      sessionW->transaction().commit();
    }

    // 2. Read sample data
    for ( int iRead=0; iRead<=1; iRead++ )
    {
      std::cout << "_TEST Read sample data" << std::endl;
      std::string connectR = UrlRO;
      coral::AccessMode accessModeR = coral::ReadOnly;
      if ( iRead == 0 ) // Read Oracle (0), then Frontier (1)
      {
        connectR = connectW;
        accessModeR = coral::Update;
      }
      std::auto_ptr<coral::ISessionProxy> sessionR( connSvc.connect( connectR, accessModeR ) );
      if ( iRead == 0 ) // Read Oracle (0), then Frontier (1)
      {
        sessionR->transaction().start();
      }
      else
      {
        sessionR->transaction().start( true );
      }
      readWithAlias( sessionR, "" );
      readWithAlias( sessionR, "t" );
      readWithAlias( sessionR, "T" );
      readCmsAlias( sessionR, TABLE2 );
      sessionR->transaction().commit();
      std::cout << "_TEST Completed successfully" << std::endl;
    }
  }
  
  catch ( std::exception& e ) {
    std::cerr << "Standard C++ exception caught: " << e.what() << std::endl;
    return 1;
  }

  catch ( ... ) {
    std::cerr << "Unknown exception caught" << std::endl;
    return 1;
  }

  return 0;

}
