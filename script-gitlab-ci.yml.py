#!/usr/bin/env python3
def build_gitlabci(binary_tag, view, OS):

  if "dbg" in binary_tag:
      return f"""build_{binary_tag}_{view}:
  tags:
    - {OS}
  variables:
    LCG_RELEASE_PATH: /cvmfs/sft-nightlies.cern.ch/lcg/views/dev3/latest/{binary_tag}
    CMAKE_ARGUMENTS: -DCMAKE_BUILD_TYPE=Debug -DLCG_python3=on -DBINARY_TAG={binary_tag}
    BINARY_TAG: {binary_tag}
  <<: *build_job

"""
  else:
      return f"""build_{binary_tag}_{view}:
  tags:
    - {OS}
  variables:
    LCG_RELEASE_PATH: /cvmfs/sft-nightlies.cern.ch/lcg/views/dev3/latest/{binary_tag}
    CMAKE_ARGUMENTS: -DCMAKE_BUILD_TYPE=Release -DLCG_python3=on -DBINARY_TAG={binary_tag}
    BINARY_TAG: {binary_tag}
  <<: *build_job

"""

platforms=['dev3-x86_64-centos7-gcc11-dbg', 'dev3-x86_64-centos7-gcc11-opt',
           'dev3-x86_64-el9-gcc11-opt']
 
with open('.gitlab-ci.yml', 'w') as f:
  f.write("""---
# Stages for the CI build.
stages: 
  - build

# Set the behaviour of the CI build.
variables:
  GIT_STRATEGY: fetch
  GIT_SUBMODULE_STRATEGY: recursive

# Common settings for all of the jobs.
before_script:
  - export MAKEFLAGS="-j`nproc` -l`nproc`"

# Template for all build jobs.
.build_template: &build_job
  stage: build
  script:
    - "source ${LCG_RELEASE_PATH}/setup.sh"
    - "cmake ${CMAKE_ARGUMENTS} -S . -B ci_build"
    - "cmake --build ci_build"
    - export CORAL_AUTH_PATH=/home/gitlab-runner/
    - export CORAL_DBLOOKUP_PATH=/home/gitlab-runner/
    - export TNS_ADMIN=/home/gitlab-runner/
    - export CORALSYS=ci_build
    - export CORAL_MSGLEVEL=Error
    - "ctest --test-dir ci_build --output-on-failure"
    - "! grep -e Failed ci_build/Testing/Temporary/LastTest.log"
  artifacts:
    paths:
      - ci_build/
    expire_in: 1 day

""")
  for platform in platforms:
    list = platform.split('-')
    f.write(build_gitlabci(platform[platform.find('-')+1:], list[0], list[2]))

