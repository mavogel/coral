CMAKE_MINIMUM_REQUIRED(VERSION 2.9...3.20)

project(CORAL)

# Sanity checks and debug printouts for new lcgcmake builds (CORALCOOL-2846)
message(STATUS "====== WELCOME TO CORAL CMakeLists.txt ======")
string(TIMESTAMP date "%Y-%m-%d %H:%M:%S")
message(STATUS "Date is ${date}")
cmake_host_system_information(RESULT host QUERY HOSTNAME)
message(STATUS "Hostname is ${host}")
message(STATUS "OS is ${CMAKE_HOST_SYSTEM_NAME} ${CMAKE_HOST_SYSTEM_VERSION}")
message(STATUS "CMAKE_COMMAND is ${CMAKE_COMMAND}")
message(STATUS "=============================================")

# Require BINARY_TAG to be set a priori (CORALCOOL-2846).
# [Do not set BINARY_TAG from host arch/os/compiler any more (CORALCOOL-2851)]
# [Do not set BINARY_TAG from CMTCONFIG/CMAKECONFIG any more (CORALCOOL-2850)]
IF("${BINARY_TAG} " STREQUAL " ")
  message(FATAL_ERROR "BINARY_TAG is not set")
ELSE()
  message(STATUS "BINARY_TAG is ${BINARY_TAG}") # CORAL/COOL
ENDIF()

# Print out the initial values of all other relevant variables.
# Note that the C and C++ compiler paths are usually inferred from CC and CXX.
# Most of these are only CMake variables, not environment variables, and some
# are CORAL/COOL-specific (see https://cmake.org/Wiki/CMake_Useful_Variables)
message(STATUS "Boost_ADDITIONAL_VERSIONS is ${Boost_ADDITIONAL_VERSIONS}")
message(STATUS "Boost_COMPILER is ${Boost_COMPILER}")
message(STATUS "Boost_NO_BOOST_CMAKE is ${Boost_NO_BOOST_CMAKE}")
message(STATUS "CMAKE_BINARY_DIR is ${CMAKE_BINARY_DIR}")
message(STATUS "CMAKE_BUILD_TYPE is ${CMAKE_BUILD_TYPE}")
message(STATUS "CMAKE_C_COMPILER is ${CMAKE_C_COMPILER}")
message(STATUS "CMAKE_CXX_COMPILER is ${CMAKE_CXX_COMPILER}")
message(STATUS "CMAKE_CXX_FLAGS is ${CMAKE_CXX_FLAGS}")
message(STATUS "CMAKE_INSTALL_PREFIX is ${CMAKE_INSTALL_PREFIX}")
message(STATUS "CMAKE_PREFIX_PATH [env] is $ENV{CMAKE_PREFIX_PATH}") # env only
message(STATUS "CMAKE_SOURCE_DIR is ${CMAKE_SOURCE_DIR}")
message(STATUS "CMAKE_USE_CCACHE is ${CMAKE_USE_CCACHE}") # CORAL/COOL
message(STATUS "CMAKE_USE_CCACHE [env] is $ENV{CMAKE_USE_CCACHE}") # CORAL/COOL
message(STATUS "CMAKE_VERBOSE_MAKEFILE is ${CMAKE_VERBOSE_MAKEFILE}")
message(STATUS "Python_ADDITIONAL_VERSIONS is ${Python_ADDITIONAL_VERSIONS}")
message(STATUS "LD_LIBRARY_PATH [env] is $ENV{LD_LIBRARY_PATH}")
message(STATUS "LCG_VERSION is ${LCG_VERSION}") # CORAL/COOL
message(STATUS "LCG_releases_base is ${LCG_releases_base}") # CORAL/COOL
message(STATUS "LCG_python3 is ${LCG_python3}") # CORAL/COOL
message(STATUS "=============================================")

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

# Print out a banner after any other targets are built
# Add a dependency of this target on all other targets of this project
# See http://stackoverflow.com/questions/25240105/
add_custom_target(POST_BUILD_BANNER ALL
  COMMAND ${CMAKE_COMMAND} -E echo "== CORAL build completed"
  COMMAND ${CMAKE_COMMAND} -E echo_append "== Date is "
  COMMAND date
  VERBATIM)

include(CORALBuildFlags)
include(CORALEnableCCache)
include(CORALEnableDistCC)

# enable build-time and run-time environments
include(CORALEnvironmentHandling)
coral_enable_env()

# compiler-related link setup and runtime environment (CORALCOOL-2853)
# - set CC and CXX (with absolute real paths) in the runtime environment
# - guess the real path to stdc++.so, set it in rpath and LD_LIBRARY_PATH
#   (now reenabled also for clang, see CORALCOOL-2855)
#   (skip this for icc, see CORALCOOL-2797 - FIXME!)
#   (skip this for Darwin, see CORALCOOL-2806 - FIXME?)
# - no longer set the PATH to the compilers (use $CC and $CXX with full paths)
#   (exclude /usr/bin if you add it back, see CORALCOOL-2803)
get_filename_component(compiler_cc_path ${CMAKE_C_COMPILER} REALPATH)
get_filename_component(compiler_cxx_path ${CMAKE_CXX_COMPILER} REALPATH)
coral_build_and_release_env(SET CC ${compiler_cc_path})
coral_build_and_release_env(SET CXX ${compiler_cxx_path})
IF(NOT CMAKE_HOST_SYSTEM_NAME MATCHES "Darwin" AND NOT BINARY_TAG MATCHES "icc")
  include(GuessCompilerRuntimePaths)
  GuessCompilerRuntimePaths(compiler_lib_path compiler_bin_path)
  # this ensures that the compiler libraries are in the rpath
  link_directories(${compiler_lib_path})
  # this is needed by test runtime also for lcg-xxx-yyy builds
  coral_build_and_release_env(PREPEND LD_LIBRARY_PATH ${compiler_lib_path})
ENDIF()

enable_testing()

# declare install locations
if (CMAKE_INSTALL_PREFIX STREQUAL /usr/local)
  # Allow overriding via "-DCMAKE_INSTALL_PREFIX=xxx"
  # [NB The default value is not "", it is "/usr/local" (see CMakeCache.txt)]
  set(CMAKE_INSTALL_PREFIX ${CMAKE_SOURCE_DIR}/${BINARY_TAG})
else()
  # Resolve symlinks in user-provided CMAKE_INSTALL_PREFIX (CORALCOOL-2836)
  get_filename_component(CMAKE_INSTALL_PREFIX_name ${CMAKE_INSTALL_PREFIX} NAME)
  get_filename_component(CMAKE_INSTALL_PREFIX_parent ${CMAKE_INSTALL_PREFIX} DIRECTORY)
  file(MAKE_DIRECTORY ${CMAKE_INSTALL_PREFIX_parent})
  get_filename_component(CMAKE_INSTALL_PREFIX_parent ${CMAKE_INSTALL_PREFIX_parent} REALPATH)
  set(CMAKE_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX_parent}/${CMAKE_INSTALL_PREFIX_name})
endif()
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# globally required headers
find_package(Boost REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})

# Add PATHs to cmake, ninja
# [NB do this first so all other paths are prepended to these (CORALCOOL-2803)]
get_filename_component(_path ${CMAKE_MAKE_PROGRAM} PATH) # PATH to ninja
if (NOT _path STREQUAL /usr/bin)
  coral_build_and_release_env(PREPEND PATH ${_path})
endif()
get_filename_component(_path ${CMAKE_COMMAND} PATH) # PATH to cmake
if (NOT _path STREQUAL /usr/bin)
  coral_build_and_release_env(PREPEND PATH ${_path})
endif()

IF(CMAKE_HOST_SYSTEM_NAME MATCHES "Darwin")
  execute_process(COMMAND env -i LANG=$ENV{LANG} man -C /private/etc/man.conf -w OUTPUT_VARIABLE default_manpath OUTPUT_STRIP_TRAILING_WHITESPACE)
ELSE()
  execute_process(COMMAND env -i LANG=$ENV{LANG} man -C /etc/man.config -w OUTPUT_VARIABLE default_manpath OUTPUT_STRIP_TRAILING_WHITESPACE)
ENDIF()

coral_release_env(PREPEND PATH              ${CMAKE_INSTALL_PREFIX}/tests/bin
                  PREPEND PATH              ${CMAKE_INSTALL_PREFIX}/bin
                  PREPEND LD_LIBRARY_PATH   ${CMAKE_INSTALL_PREFIX}/tests/lib
                  PREPEND LD_LIBRARY_PATH   ${CMAKE_INSTALL_PREFIX}/lib
                  PREPEND PYTHONPATH        ${CMAKE_INSTALL_PREFIX}/tests/bin
                  PREPEND PYTHONPATH        ${CMAKE_INSTALL_PREFIX}/lib
                  PREPEND PYTHONPATH        ${CMAKE_INSTALL_PREFIX}/python
                  APPEND  MANPATH           ${default_manpath}
                  #SET     CMTCONFIG         ${BINARY_TAG} # NO (CORALCOOL-2850)
                  SET     BINARY_TAG        ${BINARY_TAG})
coral_build_env(APPEND  MANPATH         ${default_manpath}
                #SET     CMTCONFIG       ${BINARY_TAG} # NO (CORALCOOL-2850)
                SET     BINARY_TAG      ${BINARY_TAG})

# subdirectories to include
set(subdirs CoralBase CoralKernel
            RelationalAccess CoralCommon
            MonitoringService
            XMLAuthenticationService XMLLookupService
            EnvironmentAuthenticationService
            ConnectionService RelationalService
            SQLiteAccess MySQLAccess OracleAccess
            PyCoral
            Tests)

if(NOT WIN32)
  set(subdirs ${subdirs} FrontierAccess)
endif()

# CORAL SERVER subdirectories
IF(NOT CMAKE_HOST_SYSTEM_NAME MATCHES "Darwin") # CORALCOOL-2822
  set(CORAL_SERVER_subdirs CoralMonitor CoralServerBase
                           CoralStubs CoralSockets CoralAccess
                           CoralServer CoralServerProxy)
  foreach(cs_subdir ${CORAL_SERVER_subdirs})
    list(APPEND subdirs CORAL_SERVER/${cs_subdir})
  endforeach()
ENDIF()

# add the subdirectories
foreach(subdir ${subdirs})
  message(STATUS "Adding subdirectory ${subdir}")
  include_directories(src/${subdir})
  add_subdirectory(src/${subdir})
endforeach()

# add environment for external packages
get_property(packages_found GLOBAL PROPERTY PACKAGES_FOUND)
#message("${packages_found}")
foreach(pack ${packages_found})
  coral_build_and_release_env(PACKAGE ${pack})
endforeach()

# This subdir only does (build) environment for tests and it's better to
# use it after the externals.
message(STATUS "Adding subdirectory CoralTest")
add_subdirectory(src/CoralTest)

coral_build_env(PREPEND PATH              ${CMAKE_BINARY_DIR}/tests/bin
                PREPEND PATH              ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
                PREPEND LD_LIBRARY_PATH   ${CMAKE_BINARY_DIR}/tests/lib
                PREPEND LD_LIBRARY_PATH   ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}
                PREPEND PYTHONPATH        ${CMAKE_BINARY_DIR}/tests/bin
                PREPEND PYTHONPATH        ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}
                PREPEND PYTHONPATH        ${CMAKE_BINARY_DIR}/python)

if(NOT "$ENV{VERBOSE} " STREQUAL " ")
  coral_build_and_release_env(SET VERBOSE $ENV{VERBOSE})
endif()

if(NOT "$ENV{LCG_release_area} " STREQUAL " ")
  coral_build_and_release_env(SET LCG_release_area $ENV{LCG_release_area})
endif()
if(NOT "$ENV{LCG_hostos} " STREQUAL " ")
  coral_build_and_release_env(SET LCG_hostos $ENV{LCG_hostos})
endif()

coral_generate_env_conf()

configure_file(cmake/cc-run.in cc-run @ONLY)
configure_file(cmake/cc-sh.in cc-sh @ONLY)
execute_process(COMMAND chmod +x ${CMAKE_BINARY_DIR}/cc-run ${CMAKE_BINARY_DIR}/cc-sh)
file(COPY cmake/.bashrc DESTINATION env)
file(COPY cmake/xenv DESTINATION env)
file(COPY cmake/EnvConfig DESTINATION env PATTERN ".svn" EXCLUDE PATTERN "*.pyc" EXCLUDE)

file(COPY ${CMAKE_BINARY_DIR}/cc-run ${CMAKE_BINARY_DIR}/cc-sh DESTINATION ${CMAKE_BINARY_DIR}/TMP.install)
file(COPY ${CMAKE_BINARY_DIR}/env DESTINATION ${CMAKE_BINARY_DIR}/TMP.install PATTERN "*.xenv*" EXCLUDE)

configure_file(cmake/run_nightly_tests_cmake.sh.in TMP.install/run_nightly_tests_cmake.sh @ONLY)
execute_process(COMMAND chmod +x ${CMAKE_BINARY_DIR}/TMP.install/run_nightly_tests_cmake.sh)

# Workarounds for System Integrity Protection on MacOSX 10.11 (CORALCOOL-2884)
IF(CMAKE_HOST_SYSTEM_NAME MATCHES "Darwin")
  add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/bin/bash COMMAND ${CMAKE_COMMAND} -E copy /bin/bash ${CMAKE_BINARY_DIR}/bin/bash DEPENDS /bin/bash)
  add_custom_target(bin_bash ALL DEPENDS ${CMAKE_BINARY_DIR}/bin/bash)
  install(PROGRAMS ${CMAKE_BINARY_DIR}/bin/bash DESTINATION bin)
  add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/bin/env COMMAND ${CMAKE_COMMAND} -E copy /usr/bin/env ${CMAKE_BINARY_DIR}/bin/env DEPENDS /usr/bin/env)
  add_custom_target(bin_env ALL DEPENDS ${CMAKE_BINARY_DIR}/bin/env)
  install(PROGRAMS ${CMAKE_BINARY_DIR}/bin/env DESTINATION bin)
ENDIF()

# Print out a banner before any other targets are built
# Add a dependency on this target from the lowest level targets of the project
add_custom_target(PRE_BUILD_BANNER ALL
  COMMAND ${CMAKE_COMMAND} -E echo "== CORAL build starting"
  COMMAND ${CMAKE_COMMAND} -E echo_append "== Date is "
  COMMAND date
  COMMAND ${CMAKE_COMMAND} -E echo_append "== Hostname is "
  COMMAND hostname
  COMMAND ${CMAKE_COMMAND} -E echo_append "== OS is "
  COMMAND uname -sr
  COMMAND ${CMAKE_COMMAND} -E echo_append "== Starting build from "
  COMMAND pwd
  VERBATIM)
configure_file(cmake/CTestCustom.cmake ${CMAKE_BINARY_DIR})
